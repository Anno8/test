import React from "react";
import "./Link.css";

export const Link = (props) => (
  <div className="linkContainer">
    <p>{props.link}</p>
    {props.tags.map((t) => (
      <span className="tag" key={t.id}>
        {t.tagName}
      </span>
    ))}
  </div>
);

export default Link;
