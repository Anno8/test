import React from "react";

import Link from "components/Link";

export const LinksList = ({ links, error }) => (
  <>
    {/* {error && error} */}
    {error && error.message}
    {links && links.map((l) => <Link key={l.id} {...l} />)}
  </>
);

export default LinksList;
