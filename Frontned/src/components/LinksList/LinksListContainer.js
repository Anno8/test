import { connect } from "react-redux";

import { getLinks, getError } from "modules/Links/LinksSelectors";

import LinksList from "./LinksList";

export const mapStateToProps = (state) => ({
  links: getLinks(state),
  error: getError(state)
});

export default connect(mapStateToProps)(LinksList);
