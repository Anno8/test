import { connect } from "react-redux";

import { getTagSuggestions } from "modules/Tags/tagSelectors";

import { createLink } from "modules/Links/LinksActions";
import { fetchTagSuggestions } from "modules/Tags/tagActions";

import CreateLink from "./CreateLink";

const mapStateToProps = (state) => ({
  tagSuggestion: getTagSuggestions(state)
});

export default connect(mapStateToProps, { createLink, fetchTagSuggestions })(
  CreateLink
);
