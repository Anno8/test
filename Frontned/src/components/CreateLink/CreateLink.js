import React, { useState } from "react";
import { TextField, Button, Grid } from "@material-ui/core";
import "./CreateLink.css";

export const CreateLink = ({
  createLink,
  tagSuggestion,
  fetchTagSuggestions
}) => {
  const [link, setLink] = useState("");
  const [tags, setTags] = useState([]);
  const [error, setError] = useState("");

  const validURL = (str) => {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" +
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" +
        "((\\d{1,3}\\.){3}\\d{1,3}))" +
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" +
        "(\\?[;&a-z\\d%_.~+=-]*)?" +
        "(\\#[-a-z\\d_]*)?$",
      "i"
    );
    return !!pattern.test(str);
  };

  const submit = (e) => {
    e.preventDefault();
    if (!validURL(link)) {
      setError("Link is not a valid url");
      return;
    }
    if (tags.length < 1) {
      setError("At least one tag is required");
      return;
    }
    createLink({
      link,
      tags
    });
    setLink("");
    setTags([]);
  };

  const updateTags = (tagValue, tagIndex) => {
    var newTags = tags.map((t, ind) => {
      if (ind === tagIndex) {
        return {
          tagName: tagValue
        };
      }
      return t;
    });

    setTags(newTags);
  };

  return (
    <div>
      <h3>Create a link</h3>
      <form onSubmit={submit}>
        <TextField
          fullWidth
          margin="normal"
          required
          label="Link"
          onChange={(e) => {
            setLink(e.target.value);
            fetchTagSuggestions(e.target.value);
            // TODO use lodash debounce, don't make a http call every time the text changes
          }}
          value={link}
        />
        {tags.map((t, index) => (
          <Grid container key={index} justify="center">
            <Grid item xs={9} align="center">
              <TextField
                fullWidth
                margin="normal"
                required
                label="tag name"
                onChange={(e) => updateTags(e.target.value, index)}
                value={t.tagName}
              />
            </Grid>
            <Grid item xs={3} align="center">
              <Button
                onClick={() =>
                  setTags(tags.filter((obj, ind) => ind !== index))
                }
              >
                )Remove tag
              </Button>
            </Grid>
          </Grid>
        ))}
        Based on your url here are some tag suggestions{" "}
        <div>
          {tagSuggestion &&
            tagSuggestion.map((t, index) => (
              <span
                onClick={() => setTags([...tags, { tagName: t }])}
                className="suggestions"
                key={index}
              >
                {t}
              </span>
            ))}
        </div>
        <Button
          color="primary"
          variant="contained"
          onClick={() => setTags([...tags, { tagName: "" }])}
        >
          Add a new tag
        </Button>
        <br />
        <br />
        <Button color="primary" variant="contained" type="submit">
          Create
        </Button>
        {error && <p>{error}</p>}
      </form>
    </div>
  );
};

export default CreateLink;
