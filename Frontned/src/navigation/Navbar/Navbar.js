import React from "react";
import { NavLink } from "react-router-dom";
import { AppBar, Toolbar, Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  title: {
    flexGrow: 1,
    textAlign: "left"
  }
});

const linkStyling = {
  textDecoration: "none",
  color: "#fff"
};

const Navbar = ({ user, logout }) => {
  const classes = useStyles();
  return (
    <AppBar position="fixed">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          <NavLink to="/" style={linkStyling}>
            Frontend
          </NavLink>
        </Typography>
        <Button color="inherit">
          {user ? (
            <NavLink to="/auth" onClick={logout} style={linkStyling}>
              <Typography>Logout {user}</Typography>
            </NavLink>
          ) : (
            <NavLink to="/auth" style={linkStyling}>
              <Typography>Login</Typography>
            </NavLink>
          )}
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
