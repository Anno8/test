import React from "react";
import { Switch, Route } from "react-router-dom";

import "./AppRoutes.css";
import HomePage from "pages/HomePage";
import NotFoundPage from "pages/NotFoundPage";
import AuthPage from "pages/AuthPage";

const App = ({ user }) => (
  <div className="content">
    <Switch>
      <Route path="/" exact component={HomePage} />
      <Route path="/auth" component={AuthPage} />
      <Route path="*" component={NotFoundPage} />
    </Switch>
  </div>
);

export default App;
