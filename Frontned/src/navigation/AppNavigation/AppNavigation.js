import React, { useEffect } from "react";
import { BrowserRouter } from "react-router-dom";

import { interceptor } from "utils/ApiCall";

import Navbar from "navigation/Navbar";
import AppRoutes from "navigation/AppRoutes";

const App = ({ checkUser, getToken }) => {
  useEffect(() => {
    // Check if the user has been logged in before and if the token is still valid
    checkUser();
    // register an interceptor for every http request
    return interceptor(getToken);
  }, [checkUser, getToken]);

  return (
    <BrowserRouter>
      <>
        <Navbar />
        <AppRoutes />
      </>
    </BrowserRouter>
  );
};

export default App;
