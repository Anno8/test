import React from "react";

import { shallow } from "enzyme";

import { NotFoundPage } from "../NotFoundPage";

describe("NotFoundPage tests", () => {
  const mockedProps = {
    location: {
      pathname: "test"
    }
  };
  it("Should match snapshot", () => {
    const wrapper = shallow(<NotFoundPage {...mockedProps} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("Should display pathname", () => {
    const wrapper = shallow(<NotFoundPage {...mockedProps} />);

    const headerElement = wrapper.find("h3");
    const headerElementText = headerElement.text();

    expect(headerElementText).toEqual(
      `Page not found url: ${mockedProps.location.pathname}`
    );
  });
});
