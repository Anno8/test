import { connect } from "react-redux";

import { getUsername } from "modules/Auth/AuthSelectors";

import { fetchLinks } from "modules/Links/LinksActions";

import HomePage from "./HomePage";

export const mapStateToProps = (state) => ({
  user: getUsername(state)
});

export default connect(mapStateToProps, { fetchLinks })(HomePage);
