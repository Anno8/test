import React, { useEffect } from "react";

import { Grid, makeStyles, createStyles } from "@material-ui/core";

import LinksList from "components/LinksList";
import CreateLink from "components/CreateLink";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    paper: {
      height: 140,
      width: 100
    },
    control: {
      padding: theme.spacing(2)
    }
  })
);
export const HomePage = ({ user, fetchLinks }) => {
  const classes = useStyles();
  useEffect(() => {
    if (user) {
      fetchLinks();
    }
  });

  return (
    <div>
      {user ? (
        <Grid container className={classes.root} spacing={4}>
          <Grid xs={12} sm={12} md={6} item>
            <LinksList />
          </Grid>
          <Grid xs={12} sm={12} md={6} item>
            <CreateLink />
          </Grid>
        </Grid>
      ) : (
        <h1>Please login</h1>
      )}
    </div>
  );
};

export default HomePage;
