import { REDUCER_KEY } from "./tagConstants";

export const getTagSuggestions = (state) => state[REDUCER_KEY].tagSuggestions;
