import { apiCall } from "utils/ApiCall";

import { TAG_SUGGESTIONS_ACQUIRED } from "./tagConstants";

const fetchingTagSuggestions = (tags) => ({
  type: TAG_SUGGESTIONS_ACQUIRED,
  payload: tags
});

export const clearTagSuggestions = () => (dispatch) => {
  dispatch({
    type: TAG_SUGGESTIONS_ACQUIRED
  });
};

export const fetchTagSuggestions = (link) => async (dispatch) => {
  try {
    const { data } = await apiCall({
      url: `/tags?query=${link}`,
      method: "GET",
      requiresAuth: true
    });
    dispatch(fetchingTagSuggestions(data));
  } catch (e) {
    console.log("error fetching tag suggestions", e);
  }
};
