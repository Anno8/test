export const REDUCER_KEY = "tags";

export const TAG_SUGGESTIONS_ACQUIRED = `${REDUCER_KEY}.suggestions.acquired`;
export const CLEAR_TAG_SUGGESTIONS = `${REDUCER_KEY}.clear.suggestions`;
export const INIT_STATE = {
  tagSuggestions: []
};
