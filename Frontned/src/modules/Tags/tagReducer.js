import {
  INIT_STATE,
  TAG_SUGGESTIONS_ACQUIRED,
  CLEAR_TAG_SUGGESTIONS
} from "./tagConstants";

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case TAG_SUGGESTIONS_ACQUIRED:
      return {
        ...state,
        tagSuggestions: action.payload
      };
    case CLEAR_TAG_SUGGESTIONS:
      return {
        ...state,
        tagSuggestions: []
      };
    default:
      return { ...state };
  }
};
