import {
  INIT_STATE,
  FETCHING_LINKS,
  FETCHING_LINKS_SUCCESS,
  FETCHING_LINKS_FAILURE
} from "./LinksConstants";

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCHING_LINKS:
      return {
        ...state,
        fetching: true,
        error: null
      };
    case FETCHING_LINKS_SUCCESS: {
      return {
        ...state,
        fetching: false,
        data: action.payload
      };
    }
    case FETCHING_LINKS_FAILURE: {
      return {
        ...state,
        fetching: false,
        error: action.payload
      };
    }
    default:
      return { ...state };
  }
};
