import { REDUCER_KEY } from "./LinksConstants";

export const getLinks = (state) =>
  state[REDUCER_KEY] &&
  state[REDUCER_KEY].data &&
  state[REDUCER_KEY].data.links;

export const getError = (state) => state[REDUCER_KEY].error;
