import { apiCall } from "utils/ApiCall";

import {
  FETCHING_LINKS,
  FETCHING_LINKS_SUCCESS,
  FETCHING_LINKS_FAILURE
} from "./LinksConstants";

const fetchingLinks = () => ({
  type: FETCHING_LINKS
});

const fetchingLinksSuccess = (links) => ({
  type: FETCHING_LINKS_SUCCESS,
  payload: links
});

const fetchingLinksFailure = (error) => ({
  type: FETCHING_LINKS_FAILURE,
  payload: error
});

export const createLink = (link) => async (dispatch, getState) => {
  try {
    await apiCall({
      url: "/links",
      method: "POST",
      data: link,
      requiresAuth: true
    });
    dispatch(fetchLinks());
  } catch (e) {
    console.log("Error creating link", e);
  }
};

export const fetchLinks = () => async (dispatch, getState) => {
  dispatch(fetchingLinks());
  try {
    const { data } = await apiCall({
      url: "/userspaces",
      method: "GET",
      requiresAuth: true
    });
    dispatch(fetchingLinksSuccess(data));
  } catch (e) {
    console.log("error fetching links", e);
    dispatch(fetchingLinksFailure(e));
  }
};
