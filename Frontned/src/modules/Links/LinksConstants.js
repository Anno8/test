export const REDUCER_KEY = "links";
export const FETCHING_LINKS = `${REDUCER_KEY}.fetching.links`;
export const FETCHING_LINKS_SUCCESS = `${REDUCER_KEY}.fetching.links.success`;
export const FETCHING_LINKS_FAILURE = `${REDUCER_KEY}.fetching.links.failure`;

export const INIT_STATE = {
  totalItems: 0,
  fetching: false,
  links: {
    data: {}
  },
  error: null
};
