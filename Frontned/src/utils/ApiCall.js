import axios from "axios";

import { apiUrl } from "config/constants";

const CancelToken = axios.CancelToken;
const cancelTokenSource = CancelToken.source();

const axiosInstance = axios.create({
  baseURL: apiUrl,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json"
  }
});

export const apiCall = ({ url, method, data, requiresAuth = false }) =>
  axiosInstance({
    method,
    url,
    data,
    requiresAuth
  });

export const interceptor = (getToken) => {
  axiosInstance.interceptors.request.use(async (config) => {
    if (config.requiresAuth) {
      config.cancelToken = cancelTokenSource.token;
      const token = await getToken(cancelTokenSource.cancel);
      config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
  });
};
