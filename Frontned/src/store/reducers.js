import { REDUCER_KEY as userReducerKey } from "modules/Auth/AuthConstants";
import userReducer from "modules/Auth/AuthReducer";

import { REDUCER_KEY as linkReducerKey } from "modules/Links/LinksConstants";
import linkReducer from "modules/Links/LinksReducer";

import { REDUCER_KEY as tagsReducerKey } from "modules/Tags/tagConstants";
import tagReducer from "modules/Tags/tagReducer";

export default {
  [userReducerKey]: userReducer,
  [linkReducerKey]: linkReducer,
  [tagsReducerKey]: tagReducer
};
