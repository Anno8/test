# Getting Started

* Scripts:
    * npm start -> starts the development server and serves the application on localhost:3000
    * npm run build -> packages the app for production, output is located in the build folder
    * npm run test -> runs the unit tests
    * npm run test:update -> runs the unit tests and updates snapshots if necessary
    * npm run test:watch -> runs the unit tests in watch mode
    * npm run coverage -> prints out the coverage in the terminal
    * npm run coverage:open -> displays code coverage through an html file in a browser
    * npm run lint -> goes through the code and check for linting issues.
    * npm run lint:fix -> goes through the code and check for linting issues and fixes them where possible.
    * npm run format -> runs prettier to format the files