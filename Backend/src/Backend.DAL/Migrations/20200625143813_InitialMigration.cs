﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Backend.DAL.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RefreshTokens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Token = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    ValidFor = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RefreshTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserSpaces",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSpaces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserSpaces_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Links",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Link = table.Column<string>(nullable: true),
                    UserSpaceEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Links", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Links_UserSpaces_UserSpaceEntityId",
                        column: x => x.UserSpaceEntityId,
                        principalTable: "UserSpaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    TagName = table.Column<string>(nullable: true),
                    LinkEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tags_Links_LinkEntityId",
                        column: x => x.LinkEntityId,
                        principalTable: "Links",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Links",
                columns: new[] { "Id", "Created", "Link", "Modified", "UserSpaceEntityId" },
                values: new object[] { 1, new DateTime(2020, 6, 25, 16, 38, 12, 880, DateTimeKind.Local).AddTicks(7542), "http://aleksa.com", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null });

            migrationBuilder.InsertData(
                table: "Links",
                columns: new[] { "Id", "Created", "Link", "Modified", "UserSpaceEntityId" },
                values: new object[] { 2, new DateTime(2020, 6, 25, 16, 38, 12, 880, DateTimeKind.Local).AddTicks(7573), "http://jovicic.com", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "Created", "LinkEntityId", "Modified", "TagName" },
                values: new object[] { 1, new DateTime(2020, 6, 25, 16, 38, 12, 880, DateTimeKind.Local).AddTicks(6583), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test" });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "Created", "LinkEntityId", "Modified", "TagName" },
                values: new object[] { 2, new DateTime(2020, 6, 25, 16, 38, 12, 880, DateTimeKind.Local).AddTicks(6646), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "test 1" });

            migrationBuilder.InsertData(
                table: "UserSpaces",
                columns: new[] { "Id", "Created", "Modified", "UserId" },
                values: new object[] { 1, new DateTime(2020, 6, 25, 16, 38, 12, 880, DateTimeKind.Local).AddTicks(7997), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Created", "Email", "FirstName", "LastName", "Modified", "Password", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 25, 16, 38, 12, 878, DateTimeKind.Local).AddTicks(3626), "user@gmail.com", "Pera", "Peric", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "b512d97e7cbf97c273e4db073bbb547aa65a84589227f8f3d9e4a72b9372a24d", "User" });

            migrationBuilder.CreateIndex(
                name: "IX_Links_UserSpaceEntityId",
                table: "Links",
                column: "UserSpaceEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokens_UserId",
                table: "RefreshTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_LinkEntityId",
                table: "Tags",
                column: "LinkEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSpaces_UserId",
                table: "UserSpaces",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RefreshTokens");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Links");

            migrationBuilder.DropTable(
                name: "UserSpaces");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
