﻿using System.Collections.Generic;

namespace Backend.DAL.Entities
{
    public class LinkEntity : BaseEntity
    {
        public string Link { get; set; }
        public List<TagEntity> Tags { get; set; }
    }
}
