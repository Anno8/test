﻿namespace Backend.DAL.Entities
{
    public class TagEntity : BaseEntity
    {
        public string TagName { get; set; }
    }
}
