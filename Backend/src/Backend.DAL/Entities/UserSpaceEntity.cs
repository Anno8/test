﻿using System.Collections.Generic;

namespace Backend.DAL.Entities
{
    public class UserSpaceEntity : BaseEntity
    {
        public UserEntity User { get; set; }
        public List<LinkEntity> Links { get; set; }

    }
}
