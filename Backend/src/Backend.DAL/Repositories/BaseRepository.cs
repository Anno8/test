﻿using Backend.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Backend.DAL.Repositories
{
    /// <summary>
    /// Basic CRUD functionalities to avoid duplication across repositories
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseRepository<T> where T : BaseEntity
    {
        protected readonly ApplicationDbContext DatabaseContext;
        protected readonly DbSet<T> _entities;

        protected BaseRepository(ApplicationDbContext context)
        {
            DatabaseContext = context;
            _entities = context.Set<T>();
        }

        public virtual Task<T> FindSingleByCondition(Expression<Func<T, bool>> condition)
        {
            if (condition == null)
            {
                throw new ArgumentException("Provide the condition for the query");
            }


            return _entities.AsQueryable().FirstOrDefaultAsync(condition);
        }

        public virtual async Task<List<T>> FindByCondition(Expression<Func<T, bool>> filter)
        {
            var query = _entities.AsQueryable();

            // if filter is provided add the filter to the query
            if (filter != null)
            {
                query.Where(filter);
            }

            // Count of items that match the query
            return await query.ToListAsync();
        }

        public virtual Task<T> Get(int id) => _entities.FirstOrDefaultAsync(s => s.Id == id);
        public virtual Task<List<T>> GetAll() => _entities.ToListAsync();

        public virtual async Task<T> Create(T entity)
        {
            var result = await _entities.AddAsync(entity);
            await DatabaseContext.SaveChangesAsync();
            return result.Entity;
        }

        public virtual async Task<T> Update(T entity)
        {
            if (!_entities.AsNoTracking().Any(e => e.Id == entity.Id))
                return null;

            _entities.Update(entity);
            await DatabaseContext.SaveChangesAsync();
            return entity;
        }

        public virtual async Task Delete(int id)
        {
            if (!_entities.Any(e => e.Id == id))
                return;

            _entities.Remove(_entities.Find(id));
            await DatabaseContext.SaveChangesAsync();
        }

        public virtual async Task DeleteByCondition(Expression<Func<T, bool>> filter)
        {
            var items = _entities.AsQueryable().Where(filter);

            _entities.RemoveRange(items);
            await DatabaseContext.SaveChangesAsync();
        }
    }
}
