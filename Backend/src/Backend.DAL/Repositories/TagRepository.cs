﻿using Backend.DAL.Entities;
using Backend.DAL.Interfaces;

namespace Backend.DAL.Repositories
{
    public class TagRepository : BaseRepository<TagEntity>, ITagRepository
    {
        public TagRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
