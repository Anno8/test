﻿using Backend.DAL.Entities;
using Backend.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DAL.Repositories
{
    public class LinkRepository : BaseRepository<LinkEntity>, ILinkRepository
    {
        public LinkRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Task<List<LinkEntity>> FindLinkByName(string link)
        {
            _entities.Include(r => r.Tags).Load();
            return DatabaseContext.Links.Where(l => l.Link == link).ToListAsync();
        }

        public override Task<List<LinkEntity>> GetAll()
        {
            // Forcing eager loading on foreign tables
            _entities.Include(r => r.Tags).Load();
            return base.GetAll();
        }
    }
}
