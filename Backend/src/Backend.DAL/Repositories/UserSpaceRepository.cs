﻿using Backend.DAL.Entities;
using Backend.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.DAL.Repositories
{
    public class UserSpaceRepository : BaseRepository<UserSpaceEntity>, IUserSpaceRepository
    {
        public UserSpaceRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Task<UserSpaceEntity> CreateSpace(UserEntity user)
        {
            return base.Create(new UserSpaceEntity
            {
                User = user,
                Links = new List<LinkEntity>()
            });
        }

        public Task<UserSpaceEntity> Get(string userEmail)
        {
            // Forcing eager loading on foreign tables
            _entities.Include(r => r.Links).Load();
            DatabaseContext.Links.Include(l => l.Tags).Load();
            _entities.Include(r => r.Links).Load();
            _entities.Include(r => r.User).Load();

            return base.FindSingleByCondition((us) => us.User.Email == userEmail);
        }
    }
}
