﻿using Backend.DAL.Entities;
using System.Threading.Tasks;

namespace Backend.DAL.Interfaces
{
    public interface ITagRepository
    {
        Task<TagEntity> Create(TagEntity entity);
    }
}
