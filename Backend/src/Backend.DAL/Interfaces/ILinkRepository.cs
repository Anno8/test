﻿using Backend.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.DAL.Interfaces
{
    public interface ILinkRepository
    {
        Task<List<LinkEntity>> GetAll();
        Task<LinkEntity> Create(LinkEntity entity);
        Task<List<LinkEntity>> FindLinkByName(string link);
    }
}
