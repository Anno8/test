﻿using Backend.DAL.Entities;
using System.Threading.Tasks;

namespace Backend.DAL.Interfaces
{
    public interface IUserSpaceRepository
    {
        Task<UserSpaceEntity> Get(string userEmail);

        Task<UserSpaceEntity> CreateSpace(UserEntity user);
        Task<UserSpaceEntity> Update(UserSpaceEntity user);

    }
}
