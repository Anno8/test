﻿using Backend.Api.DTOs;
using Backend.Api.Mappers;
using Backend.DAL.Entities;
using Xunit;

namespace Backend.Api.Tests.Mappers
{
    public class TagMapperTests
    {
        private readonly TagMapper _tagMapper;

        public TagMapperTests()
        {
            _tagMapper = new TagMapper();
        }


        [Fact]
        public void DtoToEntityTest()
        {
            // Arrange
            var dto = new TagDTO
            {
                Id = 1,
                TagName = "News"
            };

            // Act
            var model = _tagMapper.ToEntity(dto);

            // Assert
            Assert.Equal(dto.Id, model.Id);
            Assert.Equal(dto.TagName, model.TagName);
        }

        [Fact]
        public void EntityToDtoTest()
        {
            // Arrange
            var model = new TagEntity
            {
                Id = 1,
                TagName = "Weather"
            };

            // Act
            var dto = _tagMapper.ToDto(model);

            // Assert
            Assert.Equal(model.Id, dto.Id);
            Assert.Equal(model.TagName, dto.TagName);
        }
    }
}
