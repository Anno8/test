﻿using Backend.Core.Interfaces;
using Backend.DAL.Entities;
using Backend.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Core.Services
{
    public class LinkService : ILinkService
    {

        private readonly ILinkRepository _linkRepository;
        private readonly IUserSpaceRepository _userSpaceRepository;
        private readonly ITagRepository _tagRepository;

        public LinkService(ILinkRepository linkRepository, IUserSpaceRepository userSpaceRepository, ITagRepository tagRepository)
        {
            _linkRepository = linkRepository;
            _userSpaceRepository = userSpaceRepository;
            _tagRepository = tagRepository;
        }

        public async Task<LinkEntity> Create(LinkEntity link, string userEmail)
        {
            foreach (var tag in link.Tags)
            {
                await _tagRepository.Create(tag);
            }
            var linkEntity = await _linkRepository.Create(link);


            var userSpaceEntity = await _userSpaceRepository.Get(userEmail);

            userSpaceEntity.Links.Add(linkEntity);

            await _userSpaceRepository.Update(userSpaceEntity);
            return linkEntity;
        }

        public async Task<List<LinkEntity>> GetAllByLinkName(string link)
        {
            return await _linkRepository.FindLinkByName(link);
        }
    }
}
