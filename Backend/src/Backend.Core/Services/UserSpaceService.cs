﻿using Backend.Core.Interfaces;
using Backend.DAL.Entities;
using Backend.DAL.Interfaces;
using System.Threading.Tasks;

namespace Backend.Core.Services
{
    public class UserSpaceService : IUserSpaceService
    {
        private readonly IUserSpaceRepository _userSpaceRepository;
        public UserSpaceService(IUserSpaceRepository userSpaceRepository)
        {
            _userSpaceRepository = userSpaceRepository;
        }

        public Task<UserSpaceEntity> CreateSpace(UserEntity user)
        {
            return _userSpaceRepository.CreateSpace(user);
        }

        public Task<UserSpaceEntity> Get(string userEmail) => _userSpaceRepository.Get(userEmail);
    }
}
