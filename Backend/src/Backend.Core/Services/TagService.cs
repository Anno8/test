﻿using Backend.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Core.Services
{
    public class TagService : ITagService
    {
        private readonly ILinkService _linkService;
        public TagService(ILinkService linkService)
        {
            _linkService = linkService;
        }
        public async Task<List<string>> GetSuggestedTagsForLink(string link)
        {
            var matchingLinks = await _linkService.GetAllByLinkName(link);
            var allTags = matchingLinks.SelectMany(l => l.Tags);

            var tags = from t in allTags
                       group t.TagName by t.TagName
                      into grp
                       select new
                       {
                           Tag = grp.Key,
                           Count = grp.Count()
                       };

            var tagSuggestions = tags.OrderByDescending(k => k.Count).Select(l => l.Tag).ToList();
            return tagSuggestions;
        }
    }
}
