﻿using Backend.DAL.Entities;
using System.Threading.Tasks;

namespace Backend.Core.Interfaces
{
    public interface IUserSpaceService
    {
        Task<UserSpaceEntity> Get(string userEmail);
        Task<UserSpaceEntity> CreateSpace(UserEntity user);
    }
}
