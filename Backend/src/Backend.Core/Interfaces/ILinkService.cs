﻿using Backend.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Core.Interfaces
{
    public interface ILinkService
    {
        Task<List<LinkEntity>> GetAllByLinkName(string link);

        Task<LinkEntity> Create(LinkEntity link, string userEmail);
    }
}
