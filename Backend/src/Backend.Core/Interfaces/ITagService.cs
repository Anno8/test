﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Core.Interfaces
{
    public interface ITagService
    {
        Task<List<string>> GetSuggestedTagsForLink(string link);
    }
}
