﻿using Backend.Core.Interfaces;
using Backend.Core.Services;
using Backend.DAL.Entities;
using Backend.DAL.Interfaces;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Backend.Core.Tests.Services
{
    public class UserSpaceServiceTests
    {
        private readonly IUserSpaceService _userSpaceService;
        private readonly Mock<IUserSpaceRepository> _userSpaceRepositoryMock;

        public UserSpaceServiceTests()
        {
            _userSpaceRepositoryMock = new Mock<IUserSpaceRepository>();
            _userSpaceService = new UserSpaceService(_userSpaceRepositoryMock.Object);
        }

        [Fact]
        public async Task Get_UserSpaceDoesNotExists_ShouldReturnNull()
        {
            // Arrange
            var userEmail = "user@gmial.com";

            _userSpaceRepositoryMock
                .Setup(x => x.Get(userEmail))
                .ReturnsAsync(() => null);

            // Act
            var result = await _userSpaceService.Get(userEmail);

            // Assert
            Assert.Null(result);
            _userSpaceRepositoryMock.Verify(x => x.Get(userEmail), Times.Once);
        }

        [Fact]
        public async Task Get_UserSpaceExists_ShouldReturnUserSpace()
        {
            // Arrange
            var userEmail = "user@gmial.com";
            var userSpace = new UserSpaceEntity
            {
                Id = 1,
                Links = new List<LinkEntity>(),
                User = new UserEntity
                {
                    Email = userEmail
                }
            };

            _userSpaceRepositoryMock
                .Setup(x => x.Get(userEmail))
                .ReturnsAsync(() => userSpace);

            // Act
            var result = await _userSpaceService.Get(userEmail);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(userSpace.User.Email, result.User.Email);
            _userSpaceRepositoryMock.Verify(x => x.Get(userEmail), Times.Once);
        }
    }
}
