﻿using Backend.Core.Interfaces;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Backend.Api.Controllers
{
    public class TagsController : BaseController
    {
        private readonly ILogger<TagsController> _logger;
        private readonly ITagService _tagService;

        public TagsController(ILogger<TagsController> logger, ITagService tagService)
        {
            _logger = logger;
            _tagService = tagService;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<List<string>>> Get(string query)
        {
            var tagSuggestions = await _tagService.GetSuggestedTagsForLink(query);

            // if we have no suggestions based on previous entries attempt and if the query is a valid http query
            // go through the http doc
            var expression = @"^(http|http(s)?://)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?";

            MatchCollection mc = Regex.Matches(query, expression);

            if (tagSuggestions.Count() == 0 && mc.Count() > 0)
            {
                HtmlWeb web = new HtmlWeb();

                var htmlDoc = web.Load(query);
                var text = htmlDoc.DocumentNode.InnerText;
                var strArray = text.Split(' ').Where(s => !string.IsNullOrWhiteSpace(s) && s.Length > 4);

                var tags = from t in strArray
                           group t by t
                           into grp
                           select new
                           {
                               Tag = grp.Key,
                               Count = grp.Count()
                           };

                return tags.Where(t => t.Count > 1).OrderByDescending(t => t.Count).Select(t => t.Tag).ToList();
            }
            return tagSuggestions;
        }
    }
}
