﻿using Backend.Api.DTOs.Response;
using Backend.Api.Mappers;
using Backend.Core.Interfaces;
using Backend.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Api.Controllers
{
    public class LinksController : BaseController
    {
        private readonly ILogger<LinksController> _logger;
        private readonly ILinkService _linkService;
        private readonly IMapper<LinkEntity, LinkDTO> _mapper;

        public LinksController(ILogger<LinksController> logger, ILinkService linkService, IMapper<LinkEntity, LinkDTO> mapper)
        {
            _logger = logger;
            _linkService = linkService;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<LinkDTO>> Create([FromBody] LinkDTO dto)
        {
            var accessToken = Request.Headers["Authorization"].ToString().Split(' ')[1];
            var decodedToken = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);
            var email = decodedToken.Claims.FirstOrDefault(claim => claim.Type == "sub")?.Value;

            var entity = _mapper.ToEntity(dto);

            var createdLink = await _linkService.Create(entity, email);

            return _mapper.ToDto(createdLink);
        }
    }
}
