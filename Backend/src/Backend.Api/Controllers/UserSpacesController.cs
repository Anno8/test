﻿using Backend.Api.DTOs.Response;
using Backend.Api.Mappers;
using Backend.Core.Interfaces;
using Backend.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Api.Controllers
{
    public class UserSpacesController : BaseController
    {
        private readonly ILogger<UserSpacesController> _logger;
        private readonly IUserSpaceService _userSpaceService;
        private readonly IMapper<UserSpaceEntity, UserSpaceResponseDTO> _userSpaceMapper;
        public UserSpacesController(ILogger<UserSpacesController> logger, IUserSpaceService userSpaceService, IMapper<UserSpaceEntity, UserSpaceResponseDTO> userSpaceMapper)
        {
            _logger = logger;
            _userSpaceService = userSpaceService;
            _userSpaceMapper = userSpaceMapper;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<UserSpaceResponseDTO>> Get()
        {
            var accessToken = Request.Headers["Authorization"].ToString().Split(' ')[1];
            var decodedToken = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);
            var email = decodedToken.Claims.FirstOrDefault(claim => claim.Type == "sub")?.Value;

            var userSpace = await _userSpaceService.Get(email);
            var userSpaceDto = _userSpaceMapper.ToDto(userSpace);
            return userSpaceDto;
        }
    }
}
