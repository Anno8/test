﻿using Backend.Api.DTOs.Request;
using Backend.Api.DTOs.Response;
using Backend.Api.Mappers;
using Backend.Api.Security;
using Backend.Core.Interfaces;
using Backend.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IRefreshTokenService _refreshTokenService;
        private readonly JwtHandler _jwtHandler;
        private readonly IMapper<UserEntity, RegisterRequestDTO> _userMapper;
        private readonly ILogger<AuthController> _logger;
        private readonly IUserSpaceService _userSpaceService;

        public AuthController(
           IUserService userService,
           IRefreshTokenService refreshTokenService,
           JwtHandler jwtHandler,
           IMapper<UserEntity, RegisterRequestDTO> userMapper,
           ILogger<AuthController> logger,
           IUserSpaceService userSpaceService
            )
        {
            _userService = userService;
            _refreshTokenService = refreshTokenService;
            _jwtHandler = jwtHandler;
            _userMapper = userMapper;
            _logger = logger;
            _userSpaceService = userSpaceService;
        }


        [HttpPost]
        [ActionName("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequestDTO model)
        {
            _logger.LogInformation("Login invoked");
            var user = await _userService.GetUser(model.Email, PasswordHasher.HashPassword(model.Password));

            if (user == null)
            {
                _logger.LogInformation("Login failed");
                return BadRequest("Invalid login request");
            }

            var (token, validFor) = _jwtHandler.CreateRefreshToken();
            var refreshToken = await _refreshTokenService.Create(token, validFor, user);

            _logger.LogDebug("Login successful");
            return Ok(CreateTokensResponse(user, refreshToken.Token));
        }


        [HttpPost]
        [ActionName("register")]
        public async Task<IActionResult> Register([FromBody] RegisterRequestDTO model)
        {
            _logger.LogInformation("Register invoked");
            var userExistsWithTheGivenEmail = await _userService.GetUserByEmail(model.Email);
            if (userExistsWithTheGivenEmail != null)
            {
                _logger.LogInformation("Register failed");
                return BadRequest("The user with the given email already exists.");
            }

            var userEntity = _userMapper.ToEntity(model);
            var user = await _userService.Create(userEntity);

            var (token, validFor) = _jwtHandler.CreateRefreshToken();
            var refreshToken = await _refreshTokenService.Create(token, validFor, user);
            var tokens = CreateTokensResponse(user, refreshToken.Token);

            _userSpaceService.CreateSpace(user);

            _logger.LogInformation("Register successful");
            return Ok(tokens);
        }

        [HttpPost]
        [ActionName("token")]
        public async Task<IActionResult> Token([FromBody] TokenRequestDTO model)
        {
            _logger.LogInformation("Token invoked");
            var storedRefreshToken = await _refreshTokenService.GetToken(model.RefreshToken);

            if (storedRefreshToken == null || storedRefreshToken.ValidFor < DateTime.Now)
            {
                _logger.LogInformation("Token failed");
                //await _refreshTokenService.RevokeToken(storedRefreshToken.Id);
                return BadRequest("Invalid refresh token, please login again.");
            }

            var (token, validFor) = _jwtHandler.CreateRefreshToken();
            storedRefreshToken.ValidFor = validFor;
            storedRefreshToken.Token = token;

            var updatedRefreshToken = await _refreshTokenService.Update(storedRefreshToken);
            var tokens = CreateTokensResponse(updatedRefreshToken.User, updatedRefreshToken.Token);

            _logger.LogInformation("Token successful");
            return Ok(tokens);
        }


        [HttpPost]
        [ActionName("logout")]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            _logger.LogInformation("Logout invoked");
            var accessToken = Request.Headers["Authorization"].ToString().Split(' ')[1];
            var decodedToken = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);
            var email = decodedToken.Claims.FirstOrDefault(claim => claim.Type == "sub")?.Value;

            await _refreshTokenService.RevokeToken(email);
            _logger.LogInformation("User logged out.");
            return Ok();
        }


        private TokenResponseDTO CreateTokensResponse(UserEntity user, string refreshToken) => new TokenResponseDTO
        {
            AccessToken = _jwtHandler.CreateAccessToken(user),
            RefreshToken = refreshToken
        };
    }
}
