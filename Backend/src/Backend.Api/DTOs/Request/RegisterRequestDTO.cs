﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Backend.Api.DTOs.Request
{
    public class RegisterRequestDTO : IValidatableObject
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(UserName))
                yield return new ValidationResult("Missing User name.");
            if (string.IsNullOrWhiteSpace(Email))
                yield return new ValidationResult("Missing Email.");
            if (!new EmailAddressAttribute().IsValid(Email))
                yield return new ValidationResult("Must provide a valid email address.");
            if (string.IsNullOrWhiteSpace(FirstName))
                yield return new ValidationResult("Missing first name.");
            if (string.IsNullOrWhiteSpace(LastName))
                yield return new ValidationResult("Missing last name.");
            if (string.IsNullOrWhiteSpace(Password))
                yield return new ValidationResult("Missing password.");
            if (string.IsNullOrWhiteSpace(ConfirmPassword))
                yield return new ValidationResult("Missing confirmation password.");
            if (Password != ConfirmPassword)
                yield return new ValidationResult("Password and confirm password must match.");
        }
    }
}
