﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Backend.Api.DTOs.Response
{
    public class LinkDTO : IValidatableObject
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public List<TagDTO> Tags { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Link))
                yield return new ValidationResult("Missing link");
            if (Tags.Count() < 1)
                yield return new ValidationResult("Must at least have 1 tag");
        }
    }
}
