﻿namespace Backend.Api.DTOs
{
    public class TagDTO
    {
        public int Id { get; set; }
        public string TagName { get; set; }
    }
}
