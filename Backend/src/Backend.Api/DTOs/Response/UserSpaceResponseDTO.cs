﻿using System.Collections.Generic;

namespace Backend.Api.DTOs.Response
{
    public class UserSpaceResponseDTO
    {
        public int Id { get; set; }
        public List<LinkDTO> Links { get; set; }
    }
}
