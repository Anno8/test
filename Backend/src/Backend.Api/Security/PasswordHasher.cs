﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Backend.Api.Security
{
    public class PasswordHasher
    {
        // Naive hashing algorithm 
        public static string HashPassword(string password)
        {
            using (var algorithm = SHA256.Create())
            {
                var bytes = algorithm.ComputeHash(Encoding.ASCII.GetBytes(password));
                return string.Concat(bytes.Select(b => b.ToString("x2")));
            }
        }
    }
}
