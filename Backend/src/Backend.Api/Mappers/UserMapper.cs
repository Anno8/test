﻿using Backend.Api.DTOs.Request;
using Backend.Api.Security;
using Backend.DAL.Entities;
using System;

namespace Backend.Api.Mappers
{
    public class UserMapper : IMapper<UserEntity, RegisterRequestDTO>
    {
        public RegisterRequestDTO ToDto(UserEntity model)
        {
            throw new NotImplementedException();
        }

        public UserEntity ToEntity(RegisterRequestDTO dto)
        {
            return new UserEntity
            {
                Email = dto.Email,
                UserName = dto.UserName,
                Password = PasswordHasher.HashPassword(dto.Password),
                FirstName = dto.FirstName,
                LastName = dto.LastName
            };
        }
    }
}
