﻿using Backend.Api.DTOs.Response;
using Backend.DAL.Entities;
using System;
using System.Linq;

namespace Backend.Api.Mappers
{
    public class UserSpaceMapper : IMapper<UserSpaceEntity, UserSpaceResponseDTO>
    {
        private readonly IMapper<LinkEntity, LinkDTO> _linkMapper;
        public UserSpaceMapper(IMapper<LinkEntity, LinkDTO> linkMapper)
        {
            _linkMapper = linkMapper;
        }
        public UserSpaceResponseDTO ToDto(UserSpaceEntity model) => new UserSpaceResponseDTO
        {
            Id = model.Id,
            Links = model.Links.Select(_linkMapper.ToDto).ToList()
        };

        public UserSpaceEntity ToEntity(UserSpaceResponseDTO dto)
        {
            throw new NotImplementedException();
        }
    }
}
