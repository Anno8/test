﻿using Backend.Api.DTOs;
using Backend.Api.DTOs.Response;
using Backend.DAL.Entities;
using System.Linq;

namespace Backend.Api.Mappers
{
    public class LinkMapper : IMapper<LinkEntity, LinkDTO>
    {
        private readonly IMapper<TagEntity, TagDTO> _tagMapper;
        public LinkMapper(IMapper<TagEntity, TagDTO> tagMapper) => _tagMapper = tagMapper;

        public LinkDTO ToDto(LinkEntity model) => new LinkDTO
        {
            Id = model.Id,
            Link = model.Link,
            Tags = model.Tags.Select(_tagMapper.ToDto).ToList()
        };

        public LinkEntity ToEntity(LinkDTO dto) => new LinkEntity
        {
            Id = dto.Id,
            Link = dto.Link,
            Tags = dto.Tags.Select(_tagMapper.ToEntity).ToList()
        };
    }
}
