﻿namespace Backend.Api.Mappers
{
    /// <summary>
    /// Generic Mapper interface for us to abide when implementing our own mappers.
    /// There are other solutions as well, for example, AutoMapper.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TDto"></typeparam>
    public interface IMapper<TEntity, TDto>
    {
        TEntity ToEntity(TDto dto);
        TDto ToDto(TEntity model);
    }
}
