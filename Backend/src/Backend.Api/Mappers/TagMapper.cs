﻿using Backend.Api.DTOs;
using Backend.DAL.Entities;

namespace Backend.Api.Mappers
{
    public class TagMapper : IMapper<TagEntity, TagDTO>
    {
        public TagDTO ToDto(TagEntity model) => new TagDTO
        {
            Id = model.Id,
            TagName = model.TagName
        };

        public TagEntity ToEntity(TagDTO dto) => new TagEntity
        {
            Id = dto.Id,
            TagName = dto.TagName
        };
    }
}
